.. _label_installation:

Installation
============

Python3_ and a C-compiler are needed to build the
underlying libraries. Install the package using 
pip_ with:

.. code-block:: bash

    pip3 install maicos

or using conda_:

.. code-block:: bash

    conda install -c conda-forge maicos 

Alternatively, if you don't have special privileges, install
the package using the ``--user`` flag:

.. code-block:: bash

    pip3 install --user maicos

.. _`Python3`: https://www.python.org
.. _`pip`: https://pypi.org/project/maicos/
.. _`conda`: https://anaconda.org/conda-forge/maicos
.. _`MDAnalysis`: https://www.mdanalysis.org/
