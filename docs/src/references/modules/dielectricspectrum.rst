.. _DielectricSpectrum:

DielectricSpectrum
##################

.. autoclass:: maicos.modules.dielectricspectrum.DielectricSpectrum
    :members:
    :undoc-members:
    :show-inheritance:
