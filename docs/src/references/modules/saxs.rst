.. _Saxs:

Saxs
####

.. autoclass:: maicos.modules.saxs.Saxs
    :members:
    :undoc-members:
    :show-inheritance:
