.. _DipoleAngle:

DipoleAngle
###########

.. autoclass:: maicos.modules.dipoleangle.DipoleAngle
    :members:
    :undoc-members:
    :show-inheritance:
