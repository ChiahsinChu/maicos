.. _TemperaturePlanar:

TemperaturePlanar
#################

.. autoclass:: maicos.modules.temperatureplanar.TemperaturePlanar
    :members:
    :undoc-members:
    :show-inheritance:
