.. _DielectricPlanar:

DielectricPlanar
################

.. autoclass:: maicos.modules.dielectricplanar.DielectricPlanar
    :members:
    :undoc-members:
    :show-inheritance:
