.. _KineticEnergy:

KineticEnergy
#############

.. autoclass:: maicos.modules.kineticenergy.KineticEnergy
    :members:
    :undoc-members:
    :show-inheritance:
