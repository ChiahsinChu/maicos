.. _userdoc-references:

Reference guides
----------------

The reference guides contain details for all the analysis modules. 
The API documentation gives details on how the
calculators and additional functions can be used from each language.

.. toctree::
    :maxdepth: 1

    modules/index
    api/index
