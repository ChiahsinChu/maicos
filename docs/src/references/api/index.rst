.. _userdoc_api:

API Documentation
=================

.. toctree::
   :maxdepth: 2

   core
   lib
